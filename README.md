# The official Remmina website

To post a new article, fork the project or create a new branch, commit your changes and submit a merge request.

- After the merge, if approved, an automatic build will start and deploy the new articles on https://remmina.org .

- Images have to be dropped in the asset directory.
