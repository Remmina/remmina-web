---
title: Remmina 1.1.2 Released!
date: 2014-12-08T08:54:00+00:00
author: Antenore Gatta
layout: single
permalink: /remmina-1-1-2-released/
excerpt: Historical Remmina blog article
categories:
  - announcement
  - News
tags:
  - release
---
[<img class="alignnone size-medium wp-image-2032" src="/assets/images/Remmina111Arch-300x225.png" alt="Remmina111Arch" width="300" height="225" srcset="/assets/images/Remmina111Arch-300x225.png 300w, /assets/images/Remmina111Arch-285x214.png 285w, /assets/images/Remmina111Arch.png 660w" sizes="(max-width: 300px) 100vw, 300px" />](/assets/images/Remmina111Arch.png)

So&#8230; HO HO HO! Yuletide is upon us, and a little present comes in the form of **Remmina 1.1.2** We cherish life and are greatful for not being one of the bugs that did not make it:

  * Fixed XDMCP and other externals plugins showing a black window
  * Removed call to non existent function preventing compilation (in some cases)
  * Make remmina compatible with both VTE 2.90 and 2.91
  * Fixed a possible crash when closing a RDP connection
  * Fixed printer sharing
  * Fixed various minor issues (external tools install dir, GTK3 fix)

Have fun, best Wishes! 🙂
