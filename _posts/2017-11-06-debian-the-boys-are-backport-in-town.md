---
title: 'Debian: The Boys Are Back(port) in Town'
date: 2017-11-06T20:27:50+00:00
author: Dario Cavedon
layout: single
author_profile: true
permalink: /debian-the-boys-are-backport-in-town/
excerpt: Remmina release announcement
categories:
  - News
tags:
  - backport
  - Debian
  - Debian 9
  - Ubuntu
---
<img class="aligncenter wp-image-2473 size-large" src="/assets/images/5190538794_3236f815b2_b-1024x680.jpg" alt="" width="640" height="425" srcset="/assets/images/5190538794_3236f815b2_b.jpg 1024w, /assets/images/5190538794_3236f815b2_b-300x199.jpg 300w, /assets/images/5190538794_3236f815b2_b-768x510.jpg 768w" sizes="(max-width: 640px) 100vw, 640px" />

In a [previous post](https://www.remmina.org/wp/remmina-1-2-will-be-available-on-debian-9-stretch/) earlier this year, it was promised **Remmina 1.2 would be available on Debian 9 “Stretch”**. Well,&nbsp;now the day has come. Remmina is available in **Debian** **testing** and **unstable**. We **thank** the lovely Debian community, especially [Matteo F. Vescovi](https://github.com/mfvescovi), who made this possible.

Good news for **Ubuntu** (and derivates) too: Remmina 1.2 will be available by default in Ubuntu 18.04 “Bionic Beaver”, a giant leap forward from the buggy and outdated Remmina 1.1 series.

_Enjoy it, and keep in touch._

Photo [&#8220;The boys are back in town&#8221;](https://flic.kr/p/8UESmb) from Graham Jowett on Flikr.
