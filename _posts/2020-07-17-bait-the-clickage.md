---
title: 
author: Allan Nordhøy
layout: single
permalink: /bait-the-clickage/
excerpt: Out riding the Italian bicycle.
categories:
  - News
tags:
  - News
---

Spas!
Back are some things, and ahead of those yet more.

8 years ago bug #1 in the modern issue tracker was posted, and held seniority among the bugs
until recently. In its astute lack of movement, one day someone came upon a pondering thought to
ask whether the relative movement of its surroundings had rendered it fixed. Its following undoing went unreported for a little while too, but here is your time in the sun bug #1, they don't make them like that anymore. We are however coming for your kind, and will follow up in 2028 with bugs new and old, for an attempted conclusion in 2036.

Yek ziman her bes niye, seyarey balindîyekem pirr ye li marmasî!
Somewhat back-to-front, or from the front, is a new Central Kurdish translation,
which may or may not be displayed the wrong way around for the next release.
1.4.8 is poised to drop in August or early September.

In similar news, Antenore is soon on his fourth child, but fear not, he isn't eating them.
Whichever the case, the relentless productivity is hopefully recursive, though going on a small break from Remmina in the form of Antenore (tmow) the physical being, for 3 weeks; and Giovanni (giox) for 10 days. Indeed the production of new Remminerds seems to have ramped up. 
Espent Tveit, the Internet great, landed a magnificent haulage in the thrilling installment of [Add support for quick connecting to RDP, VNC and SPICE from the command line](https://gitlab.com/Remmina/Remmina/-/merge_requests/2093).

From the echelons of noblemen, Benjamin Anderson donated $500 towards [multi-monitor support](https://gitlab.com/Remmina/Remmina/-/issues/376#note_381195439), which is coming along and just reached "usable". On [Liberapay](https://liberapay.com/Remmina) the 40 € weekly goal was just met, and everyone who donates or contributes is an absolute boss.

This is where one might say "upvote", "like" and "subscribe", but that sort of logic would be Reddit,
and Reddit is cancer. Fortunately the unfortunate suffering from Reddit is like having a stroke, of tar, and applying feathers to assume some self-assumed grace. The cold ugly truth is we have been Redditing, and you have been Redditing along. Don't say you were Reddited along, as even those not engaging in the Redditary foibles let us carry on in our Redditery.

https://voat.co/u/Remmina is the new alternative, and hopefully a valid refuge from Twitter can be found soon too.

Ride a bicycle, a most noble amenity in the interest of transportation.

Well earnt holidays everyone.