---
title: Actors of state
author: Allan Nordhøy
layout: single
permalink: /stateactors/
excerpt: dark learning, money
categories:
  - News
  - announcement
tags:
  - news
---

Good times.

As of late, it is not really know what is beyond the pale \
for lack of generous funding in libre software. \
Looks like state sponsored activity is on the rise. \
One could wish for more openness also about the process,
as it proven difficult to secure such funding. \
Don't be shy. If you are a small to medium sized nation with aspirations \
to have everyone talking about your efforts, get in touch.