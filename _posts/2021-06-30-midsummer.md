---
title: Midsummer mindset
author: Allan Nordhøy
layout: single
permalink: /midsummer/
excerpt: 1.4.19 released.
categories:
  - News
  - Announcement
  - Release
tags:
  - release
---

Put on your best poncho, your favourite band is back in town.
Playing classics such as: \
"UI improvements and cleanup" and "Desktop integration", but also
managing to shake it up with some technical numbers.

The ever popular "The tray icon isn't showing, help" will be available as
iconographic merch.

In an effort to stay one full point ahead of the Linux kernel in Debian,
Remmina 1.4.19 brings on artists in the form of matty-r, and cth451.

Interface fiends chasing a raw and choppy experience can now turn off
smooth scrolling in its entirety, albeit at their own peril.
Disconnection where available.

Sharing multiple folders is a [new feature](https://gitlab.com/Remmina/Remmina/-/merge_requests/2287) act.

There are many changes behind the scenes, and yet more to come from whence they
came.

Brought to you by 99 people of the people booked on [Liberapay](https://liberapay.com/Remmina) and corporate dark money from
Reddit…!? and "Dark Star" on [Open Collective](https://opencollective.com/remmina) and via other discorporate means,
without which we couldn't afford no shoes.

The pedal-assisted tour-bus is powered by Esperanto, Korean, Simplified Chinese, Finnish, Italian,
French, Hebrew, German, Turkish. Ukrainian, Asturian. Spanish, Japanese and Norwegian Bokmål.

Liner notes in the form of the [full changelog](https://gitlab.com/Remmina/Remmina/-/releases#v1.4.19)
for 1.4.19 available as gatefold issue.

The world is a picture disc spinning around its axis.
