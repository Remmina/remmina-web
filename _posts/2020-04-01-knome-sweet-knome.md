---
title: Now on KNOME
author: Knomer S.
layout: single
permalink: /knome-sweet-knome/
excerpt: Knometrics of scalers.
categories:
  - News
  - Big News
  - KNOME
  - Guise
tags:
  - Breaking
---

Remmina is happy to announce its selection as the only primary program
in the new [KNOME](https://linuxreviews.org/GNOME_And_KDE_Are_Merging_To_KNOME_-_The_Next_Generation_Linux_Desktop_Environment) desktop environment.

Remmina will function as both window manager, and the connection between
programs in the new DE.

The new and shiny product can flaunt very high RAM usage, by
requiring all possible OS libraries and runtime exception handling be loaded
at all times. Requirements have thus gone up by big numbers.

Filling this role is not a problem, as moving to the new GNOMK bugtracker
has fixed all bugs. The number of open issues is a _literal_ zero.

The newly found restrictive requirements means all of the above is now in
Remmina itself, meaning you dont't even need a high-high-end machine to run the
new release—"Honey, I'm KNOME!".

If someone close to you has one, and is running KNOME-latest-latest,
(they will tell you) with the latest hyperlocal build process of the
program you want, you can keep some of your productivity,
with nminimal drastic changes.

The short hops ensure maximal utilization of environmentalism. Yes.
Disto-hopping just made one giant leap.

To fit the new knomenklature, most of the work has gone into Remmina deciding
to name itself _Knomigna_.

Make the switch today, before tomorrow.
