---
title: Remmina Warp Edition
date: 2017-07-30T21:12:58+00:00
author: Dario Cavedon
layout: single
permalink: /remmina-warp-edition/
excerpt: Remmina release announcement
categories:
  - News
  - announcement
tags:
  - release
---
<img class="aligncenter wp-image-2287 size-full" src="/assets/images/6733698_8a3cb9b845_o.jpg" alt="" width="756" height="586" srcset="/assets/images/6733698_8a3cb9b845_o.jpg 756w, /assets/images/6733698_8a3cb9b845_o-300x233.jpg 300w" sizes="(max-width: 756px) 100vw, 756px" />

This special Remmina **Warp Edition** release features a **Multi password changer** (by <a class="m_623516762205316630user-mention" href="https://github.com/giox069" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?hl=it&q=https://github.com/giox069&source=gmail&ust=1501426638083000&usg=AFQjCNEAzItzgkowgfawC7bZuQkdsl4-pQ">@giox069</a> ), improved **RDP gateway support**, a better **SSH tunneling** and lots of bugfixes. The list of enhancements is long:

  * Brand new multi password Changer
  * Improved support for RDP gateway
  * SSH tunneling with RDP server redirection
  * Enabled “Save SSH Password” checkbox.
  * Implemented managing different user and password for RD Gateway
  * RDP: Removed server hostname DNS check
  * RDP plugin: Added password expired message and updated translations
  * Added option to turn off the floating toolbar in fullscreen Remmina
  * Added option to hide the toolbar inside a windowed Remmina
  * Added servername to warning popup
  * Implemented SSH protocol plugin
  * Various other SSH improvements
  * Domain field set as the default send submit in the profile editor
  * Host key improvements
  * Fixed the exit strategy
  * Closing Remmina after a disconnection or a main window close
  * Fixed various Snap issues
  * Fixed terminal functionality to make Remmina behave correctly with ncurses
  * Fixed multiple typos of &#8216;transfered&#8217; word
  * Added “&#8211;full-version” commandline option
  * Added snap-preload on Snapcraft to get dynamic access to /snap
  * Improved CodeTriage and Bountysource buttons
  * Small English typo (fullscreen\_on\_auto)
  * Improved German translation
  * CMake clean
  * Precedence to libfreerdp2 and winpr2 libs

Longer and more detailed list of changes [available here](https://gitlab.com/Remmina/Remmina/blob/master/CHANGELOG.archive.md).

A big thanks to our tireless developers <a class="m_623516762205316630user-mention" href="https://github.com/antenore" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?hl=it&q=https://github.com/giox069&source=gmail&ust=1501426638083000&usg=AFQjCNEAzItzgkowgfawC7bZuQkdsl4-pQ">@antenore</a> and <a class="m_623516762205316630user-mention" href="https://github.com/giox069" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?hl=it&q=https://github.com/giox069&source=gmail&ust=1501426638083000&usg=AFQjCNEAzItzgkowgfawC7bZuQkdsl4-pQ">@giox069</a>, and the precious contributions of &#8211; in **alphanumerical** order &#8211; [dfiloni](https://github.com/dfiloni), [iivorait](https://github.com/iivorait), [lnicola](https://github.com/lnicola), [mfvescovi](https://github.com/mfvescovi), [nanxiongchao](https://github.com/nanxiongchao), @transistor1, [weberhofer](https://github.com/weberhofer) and [3v1n0](https://github.com/3v1n0)_._

_Image by [Steve Rotnam on Flickr](https://flic.kr/p/AvGf)._
