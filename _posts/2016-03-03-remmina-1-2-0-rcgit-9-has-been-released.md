---
title: Remmina 1.2.0-rcgit.9 has been released!
date: 2016-03-03T08:49:29+00:00
author: Antenore Gatta
layout: single
permalink: /remmina-1-2-0-rcgit-9-has-been-released/
excerpt: Remmina release announcement
categories:
  - Release
tags:
  - release
  - remmina
---

This is a quite interesting release, featuring, mainly,
autoreconnect for the RDP protocol.

Starting from this year, each version is dedicated to a special user,
contributors or projects that contributed directly or indirectly to the
Remmina development.

This release is dedicated to our first donors:

-   Daniel Bolter
-   Richard Pendlebury
-   Michael Gruben
-   Gustavo Uceda
-   Paul Johnson
-   Nic Galler
-   Viktor Sik

Changelog
=========

[1.2.0-rcgit.9](https://github.com/FreeRDP/Remmina/tree/1.2.0-rcgit.9) (2016-02-28)
-----------------------------------------------------------------------------------

[Full
changelog](https://github.com/FreeRDP/Remmina/compare/1.2.0-rcgit.8...1.2.0-rcgit.9)

**Fixed bugs:**

-   Num lock off
    [\#389](https://gitlab.com/Remmina/Remmina/-/issues/389)

**Closed issues:**

-   Share some usage statistics dialog cannot be disabled!!!
    [\#772](https://gitlab.com/Remmina/Remmina/-/issues/772)
-   VNC plugin not found
    [\#768](https://gitlab.com/Remmina/Remmina/-/issues/768)
-   Selected file after editing is wrong
    [\#761](https://gitlab.com/Remmina/Remmina/-/issues/761)
-   SSH cursor and scrolling / display issues
    [\#760](https://gitlab.com/Remmina/Remmina/-/issues/760)
-   NumLock in RDP sessions
    [\#758](https://gitlab.com/Remmina/Remmina/-/issues/758)
-   Buttons not shown inside RDP advanced tab on small screens
    [\#757](https://gitlab.com/Remmina/Remmina/-/issues/757)
-   Can't SSH tunnel over Remmina (RDP)
    [\#756](https://gitlab.com/Remmina/Remmina/-/issues/756)
-   Remmina closes unexpectedly
    [\#753](https://gitlab.com/Remmina/Remmina/-/issues/753)
-   Feature Request: After Command (opposite to precommand)
    [\#746](https://gitlab.com/Remmina/Remmina/-/issues/746)
-   Mistake. [\#739](https://gitlab.com/Remmina/Remmina/-/issues/739)
-   Currently rdp\_event.c.o can not be built
    [\#732](https://gitlab.com/Remmina/Remmina/-/issues/732)
-   Scrolled fullscreen mode does not work
    [\#729](https://gitlab.com/Remmina/Remmina/-/issues/729)
-   RDP session is dropped from time to time, reproducible situation.
    [\#723](https://gitlab.com/Remmina/Remmina/-/issues/723)
-   RDP plugin fails to load
    [\#721](https://gitlab.com/Remmina/Remmina/-/issues/721)
-   \[Info Req\] RDP v7
    [\#719](https://gitlab.com/Remmina/Remmina/-/issues/719)
-   Suddenly fails to RDP to any server
    [\#717](https://gitlab.com/Remmina/Remmina/-/issues/717)
-   Constantly and often breaks the connection. It started about 1-2
    months ago. [\#710](https://gitlab.com/Remmina/Remmina/-/issues/710)
-   No RDP connection after latest update Ubuntu
    [\#657](https://gitlab.com/Remmina/Remmina/-/issues/657)
-   Fullscreen windows open on the monitor next to them
    [\#580](https://gitlab.com/Remmina/Remmina/-/issues/580)
-   Fullscreen multiple screens
    [\#577](https://gitlab.com/Remmina/Remmina/-/issues/577)
-   Missing controls tab from top/center of window in Gnome
    [\#481](https://gitlab.com/Remmina/Remmina/-/issues/481)
-   Alt+F4 closes Remina remote Window
    [\#125](https://gitlab.com/Remmina/Remmina/-/issues/125)

**Merged pull requests:**

-   Autoreconnect
    [\#776](https://gitlab.com/Remmina/Remmina/-/merge_requests/776)
    ([giox069](https://github.com/giox069))
-   Fix for a black border (GTK undershoot) appering from GTK 3.18
    [\#767](https://gitlab.com/Remmina/Remmina/-/merge_requests/767)
    ([giox069](https://github.com/giox069))
-   Using compact settings for RDP plugin closes Issue 757
    [\#759](https://gitlab.com/Remmina/Remmina/-/merge_requests/759)
    ([antenore](https://github.com/antenore))
-   Fixes for issue
    [\#744](https://gitlab.com/Remmina/Remmina/-/issues/744)
    [\#752](https://gitlab.com/Remmina/Remmina/-/merge_requests/752)
    ([giox069](https://github.com/giox069))
-   Adjust lenght of strings
    [\#749](https://gitlab.com/Remmina/Remmina/-/merge_requests/749)
    ([weberhofer](https://github.com/weberhofer))
-   Feature Request: After command
    [\#746](https://gitlab.com/Remmina/Remmina/-/issues/746)
    [\#748](https://gitlab.com/Remmina/Remmina/-/merge_requests/748)
    ([antenore](https://github.com/antenore))
-   Fix compiler warnings
    [\#743](https://gitlab.com/Remmina/Remmina/-/merge_requests/743)
    ([weberhofer](https://github.com/weberhofer))
-   rdp-plugin requires x11 libraries
    [\#742](https://gitlab.com/Remmina/Remmina/-/merge_requests/742)
    ([weberhofer](https://github.com/weberhofer))
-   Match remmina\_survey\_start declarations
    [\#741](https://gitlab.com/Remmina/Remmina/-/merge_requests/741)
    ([weberhofer](https://github.com/weberhofer))
-   Exec if only when "name" has been initialized
    [\#740](https://gitlab.com/Remmina/Remmina/-/merge_requests/740)
    ([weberhofer](https://github.com/weberhofer))
-   XDG -- make Remmina user data dir global
    [\#738](https://gitlab.com/Remmina/Remmina/-/merge_requests/738)
    ([antenore](https://github.com/antenore))
-   Fixes for Ubuntu 14.04 compatibility
    [\#737](https://gitlab.com/Remmina/Remmina/-/merge_requests/737)
    ([giox069](https://github.com/giox069))
-   Make tool\_hello\_world compliant with our plugins model
    [\#735](https://gitlab.com/Remmina/Remmina/-/merge_requests/735)
    ([antenore](https://github.com/antenore))
-   Added user survey, community links and fixed all deprecations
    warnings
    [\#734](https://gitlab.com/Remmina/Remmina/-/merge_requests/734)
    ([antenore](https://github.com/antenore))
-   Align library name with latest FreeRDP master
    [\#731](https://gitlab.com/Remmina/Remmina/-/merge_requests/731)
    ([weberhofer](https://github.com/weberhofer))
-   Added microphone redirection
    [\#727](https://gitlab.com/Remmina/Remmina/-/merge_requests/727)
    ([akallabeth](https://github.com/akallabeth))
-   RDP plugin update client main loop to new API
    [\#726](https://gitlab.com/Remmina/Remmina/-/merge_requests/726)
    ([akallabeth](https://github.com/akallabeth))
-   Make Remmina really exit when you choose \"Exit\" or \"Quit\"
    [\#720](https://gitlab.com/Remmina/Remmina/-/merge_requests/720)
    ([giox069](https://github.com/giox069))
-   Remove some compiler warnings
    [\#716](https://gitlab.com/Remmina/Remmina/-/merge_requests/716)
    ([jviksell](https://github.com/jviksell))
