---
title: Remmina Fullscreen Release
author: Antenore Gatta
layout: single
permalink: /remmina-fullscreen-release/
categories:
  - News
  - announcement
  - Release
tags:
  - release
---

Only a month after the [Remmina Ice Pop release](/remmina-ice-pop-release/), a new release named Fullscreen release, because our heroes have finally identified, isolated and exterminated an annoying bug we discovered many months ago.

* <b>Fix fullscreen switching with multiple displays</b> (Mathias Winterhalter, Giovanni Panozzo);
* RDP channel initialization for special device sharing (serial, COM, printers);
* Updating Remmina icon to the Yaru/Suru icon set;
* SNAP fixing dependencies for the WWW plugin;
* New stats;
* Fixing Remmina main window closing issues;
* Makes RemminaNews window modal to avoid that it steals input to the Remmina connection window;
* Building issues (SNAP and Flatpak);
* Documentation fixes by *@Cypresslin* (P.-H. Lin).

In multi-monitor setup, Remmina was not behaving correctly when going fullscreen, often switching to the opposite screen where the connection was initiated.

A big thanks to Mathias Winterhalter, Giovanni Panozzo and yours truly, for having fixed this monstrosity.

* To Install see the [Download](https://remmina.org/how-to-install-remmina/) page.
* [Release announcement](https://gitlab.com/Remmina/Remmina/-/tags/v1.3.6).
* [Full changelog](https://gitlab.com/Remmina/Remmina/raw/master/CHANGELOG.md)
