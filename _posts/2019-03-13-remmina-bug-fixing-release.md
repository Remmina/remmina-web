---
title: Remmina v1.3.4 Bug-Fixing Release
author: Antenore Gatta
layout: single
permalink: /remmina-v1-3-4-bug-fixing-release/
excerpt: Remmina release announcement
categories:
  - announcement
  - News
tags:
  - release
---

A new version to fix the [#1850](https://gitlab.com/Remmina/Remmina/issues/1850) issue, "SSH public key cannot be imported: Access denied for 'none'. Authentication that can continue: publickey".

With this release, thanks to [Ken Vandine](http://ken.vandine.org/), from [Canonical](https://canonical.com), we have a new shining Snap package.

Ken has updated the whole Snap building system, and updated the core to use core18 and gnome-3-28-1804.

Great work Ken. Thanks a lot.

[Full changelog](https://gitlab.com/Remmina/Remmina/compare/v1.3.3...v1.3.4)

* Updated to use core18 and gnome-3-28-1804 [!1797](https://gitlab.com/Remmina/Remmina/merge_requests/1797) *@kenvandine*
* Snap: Build snap in CI and publish to the edge channel for builds against master [!1810](https://gitlab.com/Remmina/Remmina/merge_requests/1810) *@kenvandine*
* Resolve "SSH public key cannot be imported: Access denied for 'none'. Authentication that can continie: publickey" [!1811](https://gitlab.com/Remmina/Remmina/merge_requests/1811) *@antenore*
* Snap: Ensure the icon is installed [!1812](https://gitlab.com/Remmina/Remmina/merge_requests/1812) *@kenvandine*
