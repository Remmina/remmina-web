---
title: Running remmina on Windows
date: 2018-07-18T10:35:40+00:00
author: Antenore Gatta
layout: single
permalink: /remmina-on-windows/
excerpt: How to install Remmina on Windows
categories:
  - howto
  - News
tags:
  - windows
  - wsl
  - howto
gallery:
  - url: /assets/images/donofrio_2.jpg
    image_path: /assets/images/donofrio_2.jpg
  - url: /assets/images/donofrio_3.jpg
    image_path: /assets/images/donofrio_3.jpg
  - url: /assets/images/donofrio_4.jpg
    image_path: /assets/images/donofrio_4.jpg
  - url: /assets/images/donofrio_5.jpg
    image_path: /assets/images/donofrio_5.jpg
---

{% include figure image_path="/assets/images/donofrio_1.jpg" alt="See larger image" caption="Our beloved Remminer Lewis Donofrio' working station." %}

Many Remminers are forced to use Microsoft Windows, and often ask for a Remmina version for Windows to mediate the situation.

This is not yet possible, as Remmina still depends on many GTK+/GDK functions not available on Microsoft Windows; for which we would have to rewrite a good amount of code.

End of story? Obviously not, this is not a joke post.

One of our beloved Remminers, Lewis Donofrio, [teaches](https://www.tinyurl.com/donofrioworkdesk) how to make due in a Windows environment.

Lewis managed to install a full Ubuntu with Windows System for Linux, and he uses Remmina as one of his main tools to administers many servers.

For those of you who do not know what [Linux Subsystem for Windows](https://blogs.msdn.microsoft.com/wsl/2016/04/22/windows-subsystem-for-linux-overview/) is, this passage comes from Wikipedia:

<blockquote>
Windows Subsystem for Linux (WSL) is a compatibility layer for running Linux binary executables (in ELF format) natively on Windows . WSL provides a Linux-compatible kernel interface developed by Microsoft (containing no Linux kernel code), which can then run a GNU userland on top of it, such as that of Ubuntu, openSUSE, SUSE Linux Enterprise Server, Debian and Kali Linux. Such a userland might contain a Bash shell and command language, with native GNU/Linux command-line tools (sed, awk, etc.) and programming language interpreters (Ruby, Python, etc.).
When introduced with the Anniversary Update, only an Ubuntu image was available. The Fall Creators Update moved the installation process for Linux distributions to the Windows Store, and introduced Fedora and Suse images.
</blockquote>

The resulting functionality is impressive.

{% include gallery caption="**Remmina** on WSL" %}

Thanks and cheers to Lewis.
