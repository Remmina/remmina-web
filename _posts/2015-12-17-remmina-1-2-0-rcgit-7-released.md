---
title: Remmina 1.2.0.rcgit.7 Released!
date: 2015-12-17T16:34:23+00:00
author: Antenore Gatta
layout: single
permalink: /remmina-1-2-0-rcgit-7-released/
excerpt: Remmina 1.2.0.rcgit.7 Release announcement
categories:
  - Release
tags:
  - release
  - remmina
---
In the last few weeks, several bugs were fixed, [Remmina](https://www.remmina.org/) was enhanced with new features, and the website updated.

# Changelog

## [1.2.0-rcgit.7](https://github.com/FreeRDP/remmina/tree/1.2.0-rcgit.7) (2015-12-17)

[Full Changelog](https://github.com/FreeRDP/remmina/compare/1.2.0-rcgit.6...1.2.0-rcgit.7)

**Implemented enhancements:**

  * Icons not shown at the correct size in the main window&#8217;s listview/treeview [#701](https://gitlab.com/Remmina/Remmina/-/issues/701)
  * Reduce main window icons size fixes #701 [#702](https://gitlab.com/Remmina/Remmina/-/merge_requests/702) ([antenore](https://github.com/antenore))

**Closed issues:**

  * VNC window immediately closes after connection attempt [#699](https://gitlab.com/Remmina/Remmina/-/issues/699)

**Merged pull requests:**

  * Moved to Markdown and updated text. [#705](https://gitlab.com/Remmina/Remmina/-/merge_requests/705) ([antenore](https://github.com/antenore))
  * VNC exit with 8bpp issue699 [#704](https://gitlab.com/Remmina/Remmina/-/merge_requests/704) ([antenore](https://github.com/antenore))
  * Temporay fixes #699 &#8211; Set default color depth to 15 (high colors) [#703](https://gitlab.com/Remmina/Remmina/-/merge_requests/703) ([antenore](https://github.com/antenore))

## [1.2.0-rcgit.6](https://github.com/FreeRDP/remmina/tree/1.2.0-rcgit.6) (2015-12-10)

[Full Changelog](https://github.com/FreeRDP/remmina/compare/1.2.0-rcgit.5...1.2.0-rcgit.6)

**Implemented enhancements:**

  * Migrate from libgnome-keyring to libsecret [#652](https://gitlab.com/Remmina/Remmina/-/issues/652)
  * Move ~/.remmina to a proper place (follow XDG standards) [#197](https://gitlab.com/Remmina/Remmina/-/issues/197)
  * X2goplugin refactoring closes #603 [#655](https://gitlab.com/Remmina/Remmina/-/merge_requests/655) ([antenore](https://github.com/antenore))

**Closed issues:**

  * Remmina windows open behind other desktop windows [#691](https://gitlab.com/Remmina/Remmina/-/issues/691)
  * Indicator menu gone? [#688](https://gitlab.com/Remmina/Remmina/-/issues/688)
  * fatal error: gst/gstconfig.h: No such file or directory [#678](https://gitlab.com/Remmina/Remmina/-/issues/678)
  * remmina in gnome wayland _XInternAtom(): Remmina killed by SIGSEGV [#677](https://gitlab.com/Remmina/Remmina/-/issues/677)
  * remmina\_rdp\_cliprdr\_monitor\_ready(): Remmina killed by SIGSEGV [#676](https://gitlab.com/Remmina/Remmina/-/issues/676)
  * remmina\_rdp\_event\_release\_key(): Remmina killed by SIGSEGV [#675](https://gitlab.com/Remmina/Remmina/-/issues/675)
  * remmina: remmina\_rdp\_event\_update\_scale_factor(): Remmina killed by SIGSEGV [#674](https://gitlab.com/Remmina/Remmina/-/issues/674)
  * remmina: ringbuffer_destroy(): Remmina killed by SIGSEGV [#673](https://gitlab.com/Remmina/Remmina/-/issues/673)
  * remmina: remmina\_connection\_holder\_toolbar\_preferences_popdown(): Remmina killed by SIGSEGV [#672](https://gitlab.com/Remmina/Remmina/-/issues/672)
  * remmina: \_g\_log_abort(): Remmina killed by SIGTRAP [#671](https://gitlab.com/Remmina/Remmina/-/issues/671)
  * Concurrent remote desktop sessions on Win XP Pro [#670](https://gitlab.com/Remmina/Remmina/-/issues/670)
  * Optimize screen space usage [#661](https://gitlab.com/Remmina/Remmina/-/issues/661)
  * clipboard not synchonizing between RDP sessions and host [#556](https://gitlab.com/Remmina/Remmina/-/issues/556)
  * FTBS 1.0.0 : ld &#8211; undefined reference to symbol &#8216;g\_module\_symbol&#8217; &#8212; links.txt missing necessary libraries [#182](https://gitlab.com/Remmina/Remmina/-/issues/182)

**Merged pull requests:**

  * Avoiding conditional directives that break statements [#698](https://gitlab.com/Remmina/Remmina/-/merge_requests/698) ([RomeroMalaquias](https://github.com/RomeroMalaquias))
  * Fix memory leaks in RDP plugin, fix window width in Remmina\_connection\_window [#690](https://gitlab.com/Remmina/Remmina/-/merge_requests/690) ([giox069](https://github.com/giox069))
  * Disable wayland backend [#680](https://gitlab.com/Remmina/Remmina/-/merge_requests/680) ([antenore](https://github.com/antenore))
  * Migrate from libgnome-keyring to libsecret #652 &#8211; Inital import [#653](https://gitlab.com/Remmina/Remmina/-/merge_requests/653) ([antenore](https://github.com/antenore))

## [1.2.0-rcgit.5](https://github.com/FreeRDP/remmina/tree/1.2.0-rcgit.5) (2015-11-02)

[Full changelog](https://github.com/FreeRDP/remmina/compare/v1.2.0-rcgit.4...1.2.0-rcgit.5)

**Implemented enhancements:**

  * Feature request: Provide a way to disable parsing of .ssh/config [#648](https://gitlab.com/Remmina/Remmina/-/issues/648)
  * Toolbar drag and drop, for issue #661 [#668](https://gitlab.com/Remmina/Remmina/-/merge_requests/668) ([giox069](https://github.com/giox069))

**Closed issues:**

  * RDP plugin does not load [#667](https://gitlab.com/Remmina/Remmina/-/issues/667)
  * Remmina crashed with core dump while sharing RDP folder [#659](https://gitlab.com/Remmina/Remmina/-/issues/659)
  * High DPI display scaling [#654](https://gitlab.com/Remmina/Remmina/-/issues/654)
  * RDP Plugin Issue on Raspberry PI (ARMv7) [#651](https://gitlab.com/Remmina/Remmina/-/issues/651)
  * Moving to an embedded version of FreeRDP [#599](https://gitlab.com/Remmina/Remmina/-/issues/599)
  * Please add support multi-hop SSH tunnels / read .ssh/config [#302](https://gitlab.com/Remmina/Remmina/-/issues/302)
  * ForwardAgent [#267](https://gitlab.com/Remmina/Remmina/-/issues/267)
  * SSH Tunneling [#96](https://gitlab.com/Remmina/Remmina/-/issues/96)
  * Feature Request: SSH Tunnel with no authentication [#83](https://gitlab.com/Remmina/Remmina/-/issues/83)

**Merged pull requests:**

  * Makes parsing of ~/.ssh/config optional closes #648 [#650](https://gitlab.com/Remmina/Remmina/-/merge_requests/650) ([antenore](https://github.com/antenore))
