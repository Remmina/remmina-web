---
title: Of expectation clad
author: Allan Nordhøy
layout: single
permalink: /of-expectation-clad/
excerpt: 1.4.3 released.
categories:
  - News
  - Announcement
  - Release
  - Fundraiser
tags:
  - release
---

Of expectation clad.

What if there could be a world where those things known, could yet to have be.
One where, communication of these events, as if a mere invention of,
unbeknownst, would detail and unravel it.

What if?
What if that timeline was the one we are in. In which we are, whom?
What if in this scenario we were both at one. At all?
Of expectation clad.
Reading, or writing, as it were, the fabric of our time.
That which connects us, together. That which is the world we have created.
Fixed on speculation, affixed to
> KB grabbing fixes (mostly for Wayland) !2036 (merged) @giox069

It is likely in timelines like these one really finds it, or would find it, gripping, if not for
> KB grabbing fixes (mostly for Wayland) !2036 (merged) @giox069

Where masses in an unfixed state of fanaticism, chant, on the Xorg, sometimes,
grabbed by the fanaticism of their intercession, mostly on Wayland, for
> KB grabbing fixes (mostly for Wayland) !2036 (merged) @giox069

Where one wonders, and the wonders happens, so as to, or to the tune of
> Adding FreeRDP 3 compile option and using FreeRDP tag 2.0.0 as default !2034 (merged) @antenore

And yes, these are all realities, of [1.4.3](https://gitlab.com/Remmina/Remmina/-/releases#v1.4.3), the new Remmina release.

Hypothetically, one finds already at the other side of a [donation](https://remmina.org/donations/),
you, and the greater we, us,
to be benefactors, immediately, and in the coming times—to even greater effect.
Together with 34 others in the timeline of current events in passing on Liberapay.

Of expectation clad.
