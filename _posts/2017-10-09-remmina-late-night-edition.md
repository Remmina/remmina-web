---
title: Remmina Late Night Edition
date: 2017-10-09T16:50:44+00:00
author: Dario Cavedon
layout: single
permalink: /remmina-late-night-edition/
excerpt: Remmina release announcement
categories:
  - News
  - Release
tags:
  - FreeRDP
  - rdp
  - remmina
  - SSH
---
[<img class="aligncenter wp-image-2458 size-large" title="Manhattan Full Moon" src="/assets/images/ManhattanFullMoon-1024x647.jpg" alt="" width="640" height="404" srcset="/assets/images/ManhattanFullMoon-1024x647.jpg 1024w, /assets/images/ManhattanFullMoon-300x190.jpg 300w, /assets/images/ManhattanFullMoon-768x485.jpg 768w" sizes="(max-width: 640px) 100vw, 640px" />](https://flic.kr/p/dyhePW)

Our **brave team** worked really hard till late at night (and beyond) to bring you some amazing features, amongst them:

The shining new **dynamic resolution** for RDP

The kaleidoscopic **custom SSH color schemes**

… and so many small enhancements and bugfixes.

For more info, read the [detailed changelog](https://github.com/FreeRDP/Remmina/releases/tag/v1.2.0-rcgit.21) for this release. We really hope you enjoy working with Remmina even more than before. Don&#8217;t hesitate to contact us with the good and the bad.

_Image [&#8220;Manhattan Full Moon&#8221;](https://flic.kr/p/dyhePW) by Bob Jagendorf on Flikr._
