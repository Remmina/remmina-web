---
title: Yulebord offering
author: Allan Nordhøy
layout: single
permalink: /yulebord-offering/
excerpt: 1.3.8 is released.
categories:
  - News
  - announcement
  - Release
  - Fundraiser
tags:
  - release
---

As the helmet of releases makes the round, all is served.
This time in the form of 1.3.8, a release of somewhat pertinent immediacy.
From the cold of winter, turns out not all bugs saw their bane, and 1.3.8 puts the hammer down.
The glowing embers of the [Remmina translation effort](https://hosted.weblate.org/engage/remmina/) are now alight again,
with mystical spirits tending to the fire.
[Gifts](https://remmina.org/donations/) underpin the tree of Remmina development, rooted in gratitude.

The [full changelog](https://gitlab.com/Remmina/Remmina/-/releases#v1.3.8) for 1.3.8 is an offering of moderation.

Skål. Merry Yuletide.