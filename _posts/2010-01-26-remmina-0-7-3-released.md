---
title: Remmina 0.7.3 released!
date: 2010-01-26T08:44:00+00:00
author: Antenore Gatta
layout: single
permalink: /remmina-0-7-3-released/
excerpt: Historical Remmina blog article
categories:
  - announcement
  - News
tags:
  - release
---
This is the third bugfix release of the 0.7 branch of Remmina. There are also some translation updates for the applets.
