---
title: Ubuntu 14.04 end of support
date: 2018-06-11T10:45:31+00:00
author: Dario Cavedon
layout: single
permalink: /ubuntu-14-04-end-of-support/
excerpt: Remmina announcement about Ubuntu 14.04 end of support
categories:
  - News
tags:
  - end of support
  - Trusty Tahr
  - Ubuntu
---
<img class="aligncenter wp-image-2591 size-full" src="/assets/images/tahr.jpg" alt="" width="640" height="427" srcset="/assets/images/tahr.jpg 640w, /assets/images/tahr-300x200.jpg 300w" sizes="(max-width: 640px) 100vw, 640px" />

Remmina 1.2.0-rgcit.29 will be our latest working release on Ubuntu 14.04. Ten months before the official Trusty Tahr end of life, after long internal discussion, the GTK version will be laid to rest with a bug deemed to laborious to fix. Remmina will continue to work on Ubuntu 14.04, but won’t be upgraded further, bugfixes and new features will be available only to users of different (and more recent) distributions.

The Remmina incentive is to upgrade your Ubuntu system. Seeing as Ubuntu used Remmina 0.99 for so many years, the almost brand new Remmina 1.2.0-rgcit.29 is a great improvement😀

_Photo: &#8220;Nilgiri Tahr&#8221; by Ganesh Raghunathan on [Flikr](https://flic.kr/p/pTVYPE)._
