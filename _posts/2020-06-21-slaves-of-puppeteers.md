---
title: 
author: Allan Nordhøy
layout: single
permalink: /slaves-of-puppeteers/
excerpt: 1.4.7 released.
categories:
  - News
  - Announcement
  - Release
tags:
  - release
---

What should have been, never was. Correcting instead, the cycle, is.

After a mostly bug-fixing and somewhat bug-making release,
here is a bugfix release to cancel out the universes' questions making it into the build log.
Productivity greatly increased. Naming it "1.4.6" was narrowly avoided, but this 1.4.7 is
the best 1.4.6 yet.

Also, Pinephone is doing everything right with a [PostmarketOS community edition](https://postmarketos.org/blog/2020/06/15/pinephone-postmarketos-community-edition/),
and AsteroidOS has landed support for the [device compatiblility](https://git.io/JfNsn).

For more money, but ultimately less disposable income, the [Librem 5](https://puri.sm/products/librem-5/) is ultimately a better bet for speed and the freedom of having more open chipset drivers,
and if you want more buttons, European production and connectivity, the [Dragonbox Pyra](https://www.dragonbox.de/en/pyra) has you covered.

These are exciting times, and what was possible before between supporting the enemy and undoing their ways,
is quickly becoming something you can just buy outright. We are even getting repairability and quality back.
The next step is having something anyone can use, and that very much is within reach.

The [MNT Reform](https://mntre.com/media/reform_md/2020-05-08-the-much-more-personal-computer.html) is funded, and [The PowerPC Noteboook](https://www.powerpc-notebook.org/2020/06/pcb-ready-by-september-2020-more-time-for-donations/)
at what is a temporary present, is just 5314 € short of their goal.

The non-master minds over at GitHub have decided everyone needs to change their source tree naming,
perhaps catching a haunting glimse of self-reflection in the mirror of their chopped breakfast.

The [full changelog](https://gitlab.com/Remmina/Remmina/-/releases#v1.4.7) for 1.4.7 has memorabilities. Liberare memoriae, in memoriam libertas es.

Merry mid-summer solstice.