---
title: Remmina Valentine’s Day 2018 Edition
date: 2018-02-20T09:38:22+00:00
author: Dario Cavedon
layout: single
permalink: /remmina-valentines-day-2018-edition/
excerpt: Remmina release announcement
categories:
  - News
  - Release
tags:
  - release
  - statistics
---
<img class="aligncenter wp-image-2509 size-full" src="/assets/images/26218273648_f45e59ecb6_z-1.jpg" alt="" width="640" height="427" srcset="/assets/images/26218273648_f45e59ecb6_z-1.jpg 640w, /assets/images/26218273648_f45e59ecb6_z-1-300x200.jpg 300w" sizes="(max-width: 640px) 100vw, 640px" />

**Happy Valentine’s day to everyone!** Love and Care, and… ehr… What? It’s not Valentine’s day?? Well, here **at Remmina Project we believe every day is a good day for love and care**, introducing two great features:

  * [MS-RDPEGFX support](https://gitlab.com/Remmina/Remmina/-/merge_requests/1468).
  * The long-waited stats.
  * The usual amount of bugfixes.

### **The Stats, what we collect**

First of all: No user data is collected, only some system data that helps in triaging bugs and focusing the development effort. An example of the data collected [can be found here](https://remmina.gitlab.io/remminadoc.gitlab.io/remmina__stats_8c.html).

Look for a generated site containing only summarized data.

_Photo [&#8220;Happy Valentines Day&#8221;](https://flic.kr/p/FWPvjQ) by_ Rinet _IT Australia._
