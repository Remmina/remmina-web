---
title: Remmina Ice Pop Release
author: Antenore Gatta
layout: single
permalink: /remmina-ice-pop-release/
categories:
  - News
  - announcement
  - Release
tags:
  - release
---

{% include figure image_path="/assets/images/frozen-fruit-stock-photo.jpg" alt="See larger image" caption="A fresh Ice Pop" %}

Pretty warm, isn't it?

The fresh release. 1.3.5

As [previously announced](/we-got-news-and-more/), along with many new features.

- Configurable, human readable profile filenames by Antenore Gatta

    Save your profiles where you want and with an easier way to identify the filename.

- A KWallet plugin, which can be used instead of the GNOME Keyring, by Giovanni Panozzo.

    KWallet will be activated automatically if Remmina has been compiled with `-DWITH_KF5WALLET=ON`, KWallet is installed and running.
    Be aware there are no means of migrating passwords from one wallet to the other included.

- Many notable and subtle bugs fixed by Giovanni Panozzo.

    Null pointer reference, RDP issues, Remmina connection window fixes and refactoring and other things.

- Remmina news (and announcements) widget by Antenore Gatta.

    Periodically (sporadically) a widget will be shown with news and announcements related to your Remmina version.

- Master password, to protect settings and profiles from unauthorized modifications by Antenore Gatta

    If you set it in the general preferences, Remmina will ask a passphrase each time you try to edit, delete, copy a profile or change the general settings.

- WWW plugin (web browser with authentication for Remmina) by Antenore Gatta.

    This is a replacement of the external WebKit Remmina plugin. You can save your username and password. In the future support for webdriver/selenium and company will be added.

- New color schemes for the SSH plugin.
- Preferences cleaning by Antenore Gatta
- Profile saving bug, fixed by Antenore Gatta.
- Remmina main UI improvements by Antenore Gatta.
- Typographic and wording corrections by Davy Defaud.
- Flatpak updates to use latest libs (for a better experience) by Denis Ollier.
