---
title: On the predictability of errors
author: Allan Nordhøy
layout: single
permalink: /errors/
excerpt: mistakes to be made
categories:
  - News
tags:
  - Poemelemic
  - Opinion
---

Dispose we get treated to another pompost, son!?

On occasion moments of clarity appear as insights into \
an otherwise opaque stream unconscious.

Dreams occur more flowingly ephemeral than thinking, but a thought spared \
on its very own idle circuit, sometimes goes noticed. \
In great amount, the water under the bridge is what makes the connection \
between bridged asides.

Generational gains and epistemological disrupture. \
The scope, on noise.

The unknown within, without which nothing perceptually eminates.

The job most effectively done is controlled for good level \
and types of errors to make.

Something seen undividedly nearsighted proves selffulfillingly diffuse. \
Shared is only the experienced reconciliation of all outside insight.

Nothing is known, spare for knowledge of the unknown.

The things one can get.

If you have gotten to this page wanting something else, know that \
it may exist, but that you got this instead. 404.
