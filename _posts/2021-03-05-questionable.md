---
title: Questionable
author: Allan Nordhøy
layout: single
permalink: /questionable/
excerpt: 1.4.12 is released.
categories:
  - News
  - announcement
  - Release
tags:
  - release
---

You funded it, and here it is.
Multi-monitor at long last.
Not only is it here before its official 1.5 release,
it is here so you can test it beforehand. In a release
predating even this blogpost.

You can wait still, but having funded it, odds are
you want or are using 1.4.12 right now as much as
you want it later. And 1.4.12 you get, in turn for an even
better 1.5. That sounded about right, if not a tall order to ask
if we weren't already here. :)

---

What is it you support; inquisitive questioning?
Rather, it is the questionable inherent quality.

At any level you can and should question it, and an answer
will never be ultimately valid beyond this, but
always amount to it.

Our capacity to question is endless, yet our capacity
to provide or understand the resulting reality is finite.

So you give your finite resources away to have an
infinite question of what to expect in return.

No, you get the continuation of merit, and a system
built on it, for the threat of revoking your donations
and tying it to your input.

Know that Remmina purposely is a product of inalienable
non-corruption through its inherent properties.

Based on the licensing of what it actually is, beyond
the capacity of any taker to instil ill will with it
for anyone but themselves.

Our cooperation is just one of an infinite possibility
of ways to interact. It has the validity of building exactly
as much as we have.

A system of eternal questioning, and avoiding effort wasted
in trying at either end.

Inquisitively, answers for questions that aren't the changelog of 1.4.12 you would have to look elsewhere for.
[Questions](https://gitlab.com/Remmina/Remmina/-/releases#v1.4.12) answered are only those already asked.

And like that, it happened. Your pnowack and ToolsDevler found you, through Remmina.
Bold benedictions bestowed on your behalf :)
