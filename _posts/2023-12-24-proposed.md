---
title: Proposed propulsion
author: Allan Nordhøy
layout: single
permalink: /propulsion/
excerpt: 1.4.30 released.
categories:
  - News
  - Announcement
  - Release
tags:
  - release
---

Lots of itches scratched, by lots of different people. \
You get to decide if that grants your vessel required buoyancy. \
Whether different from others or lead to new paths by example, \
making a difference is all in the making of marbled sculptors. \
If you are using macOS and wonder "What could possibly be my problem?" \
it is no longer that of building Remmina. \
Flip to the back of the book and cross that one off. \
Free citizens of the Internet get less, and are forced to share fixes throughout the whole stack — from the build environment, packaging, back-end, interaction, to UI and translations. \
Wherever the notion of rocking the bloat came from, \
your particular boat-rocking skills are highly valued.

It [takes you in](https://gitlab.com/Remmina/Remmina/-/releases#v1.4.30) with the new and out with the same old.
