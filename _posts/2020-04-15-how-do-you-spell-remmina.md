---
title: How do you spell Remmina?
author: Antenore Gatta
layout: single
permalink: /how-do-you-spell-remmina/
excerpt: Remmina release announcement
categories:
  - article
tags:
  - spell
  - article
---

{% include figure image_path="/assets/images/how-do-you-spell-it.png" %}

Remmina was born in the US more than a decade ago, if you ever wondered how to spell it, here you can find an audio file with the right pronunciation.

- [Remmina pronounce - OGG Vorbis](/assets/media/Remmina.ogg)
- [Remmina pronounce - MP3](/assets/media/Remmina.mp3)
