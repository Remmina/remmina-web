---
title: Remmina 1.2 will be available on Debian 9 “Stretch”
author: Dario Cavedon
layout: single
permalink: /remmina-1-2-will-be-available-on-debian-9-stretch/
excerpt: Remmina release announcement
categories:
  - News
tags:
  - Debian
  - Debian 9
  - news
  - Stretch
---
<img class="alignnone size-medium wp-image-2271 aligncenter" src="/assets/images/debianopenlogo-226x300.png" alt="Debian logo" width="226" height="300" srcset="/assets/images/debianopenlogo-226x300.png 226w, /assets/images/debianopenlogo.png 300w" sizes="(max-width: 226px) 100vw, 226px" />

Hello Remminers. We **love** that our software is available and updated on different systems, and have direct connection with many distribution maintainers. So, we’ve got some news from our friends at [Debian](https://www.debian.org), as they’re releasing a brand new version of their wonderful system.

**First**: <span class="il">Remmina</span> 1.1 <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=863302" target="_blank" rel="noopener noreferrer">won’t be available</a> in the upcoming **Debian 9 “Stretch”**. <span class="il">Remmina</span> 1.1 is an old and unsupported version of <span class="il">Remmina</span>.

**Closing a door, opening a window**: the solid and newest <span class="il">Remmina</span> 1.2 will soon be available on Debian 9 via backports. More details in the coming days.

_Stay tuned. 🙂_
