---
title: The fast, stable, and always free Linux SPICE client
excerpt: One of Remmina's many features is a SPICE client in the connection manager with many tunable options to get you connected just how you want
permalink: /remmina-spice/
author_profile: false
layout: single
---

One of Remmina's many features is a SPICE client in the connection manager with many tunable options to get you connected just how you want.

{% include figure image_path="/assets/images/screenshots/remmina_spice_connection.png" %}

[Simple Protocol for Independent Computing Environments - Wikipedia](https://en.wikipedia.org/wiki/Simple_Protocol_for_Independent_Computing_Environments)

> **SPICE** (the **Simple Protocol for Independent Computing Environments**) is a remote-[display](https://en.wikipedia.org/wiki/Display_device "Display device") system built for [virtual environments](https://en.wikipedia.org/wiki/Virtual_environment "Virtual environment") which allows users to view a computing ["desktop" environment](https://en.wikipedia.org/wiki/Desktop_environment) – not only on its computer-server machine, but also from anywhere on the [Internet](https://en.wikipedia.org/wiki/Internet "Internet") – using a wide variety of [machine architectures](https://en.wikipedia.org/wiki/Computer_architecture).
>
> [Qumranet](https://en.wikipedia.org/wiki/Qumranet "Qumranet") originally developed SPICE using a closed-source codebase in 2007. [Red Hat, Inc](https://en.wikipedia.org/wiki/Red_Hat "Red Hat") acquired Qumranet in 2008, and in December 2009 released the code under an [open-source license](https://en.wikipedia.org/wiki/Open-source_license "Open-source license") and made the protocol an open standard.

{% include figure image_path="/assets/images/screenshots/remmina_spice_pref_basic.png" %}

{% include figure image_path="/assets/images/screenshots/remmina_spice_pref_advanced.png" %}
