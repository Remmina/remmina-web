---
title: Make a Donation
permalink: /donations/
excerpt: "You have our gratitude in choosing copylefted libre software, donations help **Remmina**."
layout: single
classes:
  - landing
  - wide
header:
  #overlay_color: rgba(74, 147, 221, 0.8)
  #overlay_image: /assets/images/data-center.jpg
  overlay_filter: rgba(30, 30, 30, 0.8)
  caption: "Image credit: [Antenore Gatta](https://antenore.simbiosi.org)"
intro:
  - excerpt: "Your donation means the world to us. If you have a specific wish, you can tell us about it on [Matrix](https://matrix.to/#/#remmina-room:matrix.org). Thank you kind and awesome person."
author_profile: false
---

{% include feature_row id="intro" type="center" %}

## 100% funded by people like you

These are the recurring expences paid by Antenore and Giovanni. <!---TODO Update this chart once all payments are taken over by hiroyuki-->

| What                                                  | Description                                                                                                      | Yearly cost | Paid       | Renewal    |
|-------------------------------------------------------|------------------------------------------------------------------------------------------------------------------|-------------|------------|------------|
| remmina.com (OVH.COM)                                 | domain                                                                                                           | €9.99 EUR    | 15/04/2021 | 15/04/2022 |
| remmina.org (OVH.COM)                                 | domain                                                                                                           | €12.99 EUR   | 25/03/2021 | 25/03/2022 |
| remmina.org (OVH.COM)                                 | - Hosting - Performance1 - 12 months.<br>- 1000 pops, 1000 mailing lists.<br>- CDN - 3 POPs                      | €119.88 EUR  | 24/10/2020 | 23/10/2021 |
| remmina.org (OVH.COM)                                 | zone DNS                                                                                                         | €1.00 EUR    | 25/03/2020 | 25/03/2022 |
| remmina.org (Hostwinds.com)                           | VPS                                                                                                              | $83.88 USD   | 30/12/2020 | 30/12/2021 |
| Computing environment (self-owned) | 2 Intel servers and 1 pi4 are used to build Remmina packages, test different OS configurations and other tasks.                     | €180.00 EUR  |            |            |
| Total                                                 |                                                                                                                  | ~€394.88 EUR |            |            |

Honest hard work goes into building Remmina packages, testing Remmina on different operating systems and against different
remote servers (Linux and Windows VMs), managing translations, elaborating opt-in statistics, debugging reported bugs, helping users, implementing new features, and preparing new releases.

We are so grateful for the increased donations, and any stability helps focus on what is most demanded.

## <i class="fa fa-liberapay" aria-hidden="true"></i> Liberapay

[Liberapay](https://liberapay.com/Remmina/donate) is libre software too, and a way to donate to people whose work you appreciate.

 [![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/Remmina/donate)

<script async src="https://liberapay.com/Remmina/widgets/button.js"></script>
<noscript><a href="https://liberapay.com/Remmina/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript>

<img alt="Current Liberapay donation amount" src="https://img.shields.io/liberapay/receives/Remmina.svg?logo=liberapay">
<img alt="Current number of Liberapay donators" src="https://img.shields.io/liberapay/patrons/Remmina.svg?logo=liberapay">

## <i class="fa fa-open-collective"></i> Open Collective

Donations through Open Source Collective are [not tax deducible](https://www.irs.gov/charities-non-profits/other-non-profits/tax-treatment-of-donations-501c6-organizations), even though it is a 501(c)(6).

<script src="https://opencollective.com/remmina/banner.js"></script>

<img alt="open collective badge" src="https://opencollective.com/remmina/tiers/remminer/badge.svg?label=Remminer&color=brightgreen" />

[![](https://opencollective.com/remmina/tiers/badge.svg)](https://opencollective.com/remmina)

## Donors

### Remmina patrons

These kind people have donated the biggest amount of money.

- Daniel Bolter
- Benjamin Anderson ( soupcan ), known for [GoldenEye: Source](https://geshl2.com/) ( **Donated $500** )
- [Tim Richardson - GrowthPath founder](https://www.growthpath.com.au/people/growthpath-team-2)
- Rafael Wolf owner of [EITS, LLC, dba Express IT Solutions](https://eitsonline.com) Kalamazoo, MI 49048 USA. ( **Donated $150** )
- [Khalifa Alshamsi](https://twitter.com/kha1ifa__)
- Christian Houle
- Ingo Humann
- Justin Scott
- Luděk Habarta
- Kevin Salisbury
- Dr. med. dent. Thomas Ulrich
- STRUBartacus
- BH
- Anton Kruglyakov
- Lucious
- Rami Hakim

### Year 2021 donors

- Bruno Gadaleta
- /u/esmpsu89
- Dark Star
- Vinz
- Andre Steinz
- Eric Taylor
- Incognito
- MARCELO
- L.Adigard
- Antoni Aloy Torrens
- Raphael Schweikert sabberworm 
- Marwan Yassin Marware 
- dpirly
- Brendan Long brendanlong
- Gabriel Einsdorf gab1one 
- Malcolm Smith mds08011
- Vektor Prime vektorprime 
- theschitz theschitz
- Matthew Mattox mattmattox 
- Yoann YoannMourot 

Coporates:
- Babiel GmbH

#### Year 2020 donors

- Benjamin Anderson ( soupcan ), known for [GoldenEye: Source](https://geshl2.com/) ( **Donated $500** )
- Abdulkarim Malkadi Khormi
- Абрамов Павел
- Peter Vollebregt

#### Year 2019 donors

- Daniel Bolter
- [Khalifa Alshamsi](https://twitter.com/kha1ifa__)
- Christian Houle
- Ingo Humann
- Justin Scott
- Luděk Habarta
- atzk
- Abdulkarim Malkadi Khormi
- Андрей Коробков
- Andres Zanzani
- Andrew Hart
- Antonio Quinonez-Mun
- Balvas Eugene
- Benoit Mercier
- Blagoy Chepelov
- Clayton Casciato
- Clemens Eberwein
- Cordula Herzog
- Denis Đukić
- Dennis Lampe
- Denoid Tucker
- EXCOMEDIA, Inc.
- Герун Данил
- Graham Sivill
- Gunter Moegel
- Hawkins IT Group Inc
- Hector SAenz
- Исаков Андрей
- Janez Košmrlj
- Jonathan Rogers
- Josef Abdel Wahed
- Jürgen Müller
- Клюенков Алексей
- Lance Edusei
- Подловилин Виталий
- Lutz Urban
- Majed Zouhairy
- Manuel Moelgg
- Мантуров Дмитрий
- Marc Schlegel
- Marcel Pötter
- Marcel Ostrovsky
- Marcin Wolcendorf
- Marjan Bugarinović
- Marko Milosavljevic
- Markus Heimberg
- Marwan Yassin
- Matteo Biaggi
- Matthias Baumann
- Matthias Tapis
- Mathieu Morlon
- Michael Brazeau
- Márcio Oliveira Luiz
- Nils Panitzsch
- Per Arnold Blaasmo
- Petar Kulić
- Philipp Köhler
- Roman Cravtov
- Robert Premuž
- Ross Philip
- Ruston Rountree
- Sergio Omiccioli
- Thomas Stofer Computerservice
- Trond Nystuen
- Werner Mittermayer
- Welles Freire Ximenes
- Wolfgang Scherer

#### Year 2018 donors

- Anton Karmanov
- Antonio Quinonez-Mun
- CSI Dudek Marcin
- coder-zero @Future Systems bvba
- Debapriyo Sarkar
- Marco Polano
- Michael Hipp
- Peter Link
- Praveen Bhamidipati
- Radek Loutocký
- Sergey Lebfr
- Tommy Sweeney

#### Year 2017 donors

- Anonymous (16)
- Brad Kulkin
- Daniel Platteau
- Dmitry Yudin
- Fuad kamal
- František Řezáč
- Jakub Kubień
- Leigh Mutimer
- Luca Menini
- Mauro Frischherz
- Paul Walker
- Tomasz Kubacki

#### Year 2016 donors

These donors have contributed a great amount of money

- Daniel Bolter
- Bruce Cran
- Jamison Guyton
- Allen Smith
- Антонов Иван
- Artur Mroczko
- Bruce Cran
- Cesar Reza
- Christian Uceda
- Dietmar Wolf
- Gustavo Uceda
- Ivan Antonov
- Ivan Dobrianov
- Jamison Guyton
- Lars Povlsen
- Lynne Lawrence
- Mikhail Blinkov
- Michael Gruben
- Michael Schneider
- Nic Galler
- Patrick Saccani-Williams
- Paul Johnson
- Paul Zoulin
- Richard Lees
- Richard Pendlebury
- Stepan Naumov
- Tim Richardson
- Troy Lea
- Viktor Sik

## Disclaimer

The Remmina project is not a nonprofit organization. Revenue streams go towards development as directly as possible.
This avoids the biggest libre software 501(c)(6) fiscal host dictating which revenue streams can be utilized and saves 5% from the net revenue.
The money you donate, goes to one of the Remmina maintainers (rotating yearly).
For amounts above 200$, get in contact with us.
Thank you so much for your support!

Exact amounts are not disclosed for privacy, except if requested.
Ask at any time how your donation is used. Remmina is fully transparent, and is helped by hiding nothing.
If you want to be removed, or you are not listed, or you want more details about you added, let us know.
