---
title: The fast, stable, and always free Linux RDP client
excerpt: One of Remmina's many features is a RDP client in the connection manager with many tunable options to get you connected just how you want
permalink: /remmina-rdp/
author_profile: false
layout: single
---

One of Remmina's many features is a RDP client in the connection manager with many tunable options to get you connected just how you want.

{% include figure image_path="/assets/images/screenshots/remmina_RDP_connection.png" %}

[Remote Desktop Protocol - Wikipedia](https://en.wikipedia.org/wiki/Remote_Desktop_Protocol)

> **Remote Desktop Protocol** (**RDP**) is a [proprietary protocol](https://en.wikipedia.org/wiki/Proprietary_protocol "Proprietary protocol") developed by [Microsoft](https://en.wikipedia.org/wiki/Microsoft "Microsoft") which provides a user with a [graphical interface](https://en.wikipedia.org/wiki/Graphical_user_interface) to connect to another computer over a network connection.[\[1\]](https://en.wikipedia.org/wiki/Remote_Desktop_Protocol#cite_note-:0-1) The user employs RDP client software for this purpose, while the other computer must run RDP server software.

{% include figure image_path="/assets/images/screenshots/remmina_RDP_pref_basic.png" %}

{% include figure image_path="/assets/images/screenshots/remmina_RDP_pref_advanced.png" %}
