---
title: The fast, stable, and always free Linux VNC client
excerpt: One of Remmina's many features is a VNC client in the connection manager with many tunable options to get you connected just how you want
permalink: /remmina-vnc/
author_profile: false
layout: single
---

One of Remmina's many features is a VNC client in the connection manager with many tunable options to get you connected just how you want.

{% include figure image_path="/assets/images/screenshots/remmina_vnc_connection.png" %}

[Virtual Network Computing - Wikipedia](https://en.wikipedia.org/wiki/Virtual_Network_Computing)

> **Virtual Network Computing** (**VNC**) is a graphical [desktop-sharing](https://en.wikipedia.org/wiki/Desktop_sharing) system that uses the [Remote Frame Buffer protocol (RFB)](https://en.wikipedia.org/wiki/RFB_protocol "RFB protocol") to remotely control another [computer](https://en.wikipedia.org/wiki/Computer). It transmits the [keyboard](https://en.wikipedia.org/wiki/Computer_keyboard "Computer keyboard") and [mouse](https://en.wikipedia.org/wiki/Computer_mouse) input from one computer to another, relaying the graphical-[screen](https://en.wikipedia.org/wiki/Computer_screen "Computer screen") updates, over a [network](https://en.wikipedia.org/wiki/Computer_network "Computer network").

{% include figure image_path="/assets/images/screenshots/remmina_vnc_pref_basic.png" %}

{% include figure image_path="/assets/images/screenshots/remmina_vnc_pref_advanced.png" %}
