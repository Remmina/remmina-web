---
title: The fast, stable, and always free X2Go client
excerpt: One of Remmina's many features is a X2Go client in the connection manager with many tunable options to get you connected just how you want
permalink: /remmina-x2go/
author_profile: false
layout: single
---

One of Remmina's many features is an _obviously nice_ X2Go client in the connection manager with many tunable options to get you connected just how you want.

{% include figure image_path="/assets/images/screenshots/remmina_X2Go_connection.png" %}

[X2Go protocol - Wikipedia](https://en.wikipedia.org/wiki/X2Go)

> **X2Go**) is an [open protocol](https://en.wikipedia.org/wiki/Open_standard) developed by [The X2Go Organigram](https://wiki.x2go.org/doku.php/doc:organigram) which provides a user with a [graphical interface](https://en.wikipedia.org/wiki/Graphical_user_interface) to connect to another computer over a network connection.[\[1\]](https://en.wikipedia.org/wiki/Remote_Desktop_Protocol#cite_note-:0-1) The user employs X2Go client software for this purpose, while the other computer must run X2Go server software.

<!-- Needs work
{% include figure image_path="/assets/images/screenshots/remmina_X2Go_pref_basic.png" %}
{% include figure image_path="/assets/images/screenshots/x2go_logo.svg" %}
{% include figure image_path="/assets/images/screenshots/x2go_mascot.svg" %}
{% include figure image_path="/assets/images/screenshots/x2go_slogan.svg" %}
{% include figure image_path="/assets/images/screenshots/remmina_X2Go_pref_advanced.png" %}
-->
